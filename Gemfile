source 'https://rubygems.org'

##
#  Core components
##

# Rails is the framework we use.
gem 'rails', '~> 6.1.4'

# Make sure we get the latest security updates
gem 'nokogiri', '~> 1.12'

# Rake is rubys make... performing tasks
# locking in to latest major to fix API
gem 'rake', '~> 13.0', require: false

# Application preloader for faster start time
gem 'spring', group: :development

# reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '~> 1.9', require: false

# locking in to latest major to fix API
gem 'i18n', '~> 1.8'

# improved gem to access mysql database
# locking in to latest major to fix API
gem 'mysql2', '~> 0.5'

# parsing and generating JSON
# locking in to latest major to fix API
gem 'json', '~> 2.6'

# Markup language that uses indent to indicate nesting
# locking in to latest major to fix API
gem 'haml', '~> 5.0'
gem 'haml-rails', '~> 2.0'

# Extendet scriptable CSS language
# locking in to latest major to fix API
gem 'sass'

# Sprockets - included by rails by default
gem 'sprockets', '~> 4.0'

##
# Prototype - yes. we still use it.
# we use a fork which is rails 5.x compatible
# tests do not pass for this fork
gem 'prototype-rails', github: 'voxmedia/prototype-rails', ref: '740f70179a7c2a1d8619518294f75cd3755a8f68'

# Full text search for the database
gem 'thinking-sphinx', '~> 5.3'

# Enhanced Tagging lib. Used to tag pages
gem 'acts-as-taggable-on', '~> 8.1'

# Rails 5 migration
##

# ActionView::Helpers::RecordTagHelper moved to external gem
gem 'record_tag_helper', '~> 1.0'

##
# Upgrade pending
##

# Use delayed job to postpone the delta processing
# latest version available. Stick to major release
gem 'ts-delayed-delta', '~> 2.0'

# Page Caching has been removed from rails 4.
# migrate it and drop this.
gem 'actionpack-page_caching'

##
# Single use tools
##

# Pundit, permission system
# latest version available. Stick to major release
gem 'pundit', '~> 2.1'

# Bcrypt for has_secure_password
gem 'bcrypt', '~> 3.1.7'

gem 'secure_headers', '~> 6.0'

# ?
# locking in to latest major to fix API
gem 'http_accept_language', '~> 2.0'

# Removes invalid UTF-8 characters from requests
# use the latest. No API that could change.
gem 'utf8-cleaner'

# Pagination for lists with a lot of items
# locking in to latest major to fix API
gem 'will_paginate', '~> 3.1'

# state-machine for requests
# locking in to latest major to fix API
gem 'aasm', '~> 5.2'
# requested by after_commit hooks in aasm
gem 'after_commit_everywhere', '~> 1.1'

# lists used for tasks and choices in votes so far
# continuation of the old standart rails plugin
# locking in to latest major to fix API, not really maintained though
gem 'acts_as_list', '~> 1.0'

# Check the format of email addresses against RFCs
# better maintained than validates_as_email
# locking in to latest major to fix API
gem 'validates_email_format_of', '~> 1.6'

# Used to keep spammers from creating accounts
# locking in to latest major to fix API
gem 'invisible_captcha', '~>2.0'

##
## GEMS required, and compilation is required to install
##

# Formatting text input
# We extend this to resolve links locally -> GreenCloth
# locking in to latest major to fix API
gem 'RedCloth', '~> 4.2'

##
## required, included with crabgrass
##

# extension of the redcloth markup lang
gem 'greencloth', require: 'greencloth',
                  path: 'vendor/gems/riseuplabs-greencloth-0.1'

# media upload post processing has it's own repo
# version is rather strict for now as api may still change.
gem 'crabgrass_media', '~> 0.5.0', require: 'media'

##
## not required, but a really good idea
##

# detect mime-types of uploaded files
#
gem 'mime-types', require: 'mime/types'

# process heavy tasks asynchronously
# 4.0 is most recent right now. fix major version.
gem 'delayed_job_active_record', '~> 4.0'

# delayed job runner as a deamon
gem 'daemons'

# unpack file uploads
gem 'rubyzip', '~> 2.3', require: false

# load new rubyzip, but with the old API.
# TODO: use the new zip api and remove gem zip-zip
gem 'zip-zip', require: 'zip'

# gnupg for email encryption
#
gem 'mail-gpg', '~> 0.4'

# Prevent loading native net-protocol and via gems.
# (mail-gpg requires mail requires net-imap requires net-protocol)
# See https://github.com/ruby/net-imap/issues/16 .
gem 'net-http'

##
# Environment specific
##

group :production do
  gem 'mini_racer', platforms: :ruby
end

group :production, :development do
  # used to install crontab
  gem 'whenever', require: false
  # used to minify javascript
  gem 'uglifier', '>= 1.3.0', require: false
end

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  # needed for some rake tasks, but not generally.
  gem 'sdoc', require: false

  # sdoc requires rdoc
  # rdoc 6.4 requires psych 4.0 which causes all kinds of issues.
  gem 'rdoc', '~> 6.3.0'
end

group :test do
  gem 'simplecov', require: false
end

group :test, :development do
  gem 'byebug'
end

gem 'web-console', group: :development

group :test, :ci do
  ##
  ## TESTS
  ##

  gem 'factory_bot_rails'
  gem 'faker', '~> 2.19'

  # temporary fix for minitest 5.11 issue
  gem 'minitest', '~>5.10', require: false

  # contains helper methods like assigns and assert_template
  gem 'rails-controller-testing'

  ##
  ## INTEGRATION TESTS
  ##

  gem 'capybara', require: false

  # Capybara driver with javascript capabilities using chromes CDP
  gem 'apparition', github: 'twalpole/apparition', ref: 'ca86be4d54af835d531dbcd2b86e7b2c77f85f34'

  # The castle_gates tests are based on sqlite
  gem 'sqlite3'
end

gem 'bundler-audit'
